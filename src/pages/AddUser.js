import React, { useState } from 'react'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button'
import Alert from '@mui/material/Alert'
import { Container } from '@mui/system';
import { useNavigate } from 'react-router';
import { useDispatch } from 'react-redux';
import { addUser } from '../redux/actions';

const AddUser = () => {
  const [state, setState] = useState({
    name: "",
    address: "",
    email: "",
    contact: ""
  })

  const [error, setError] = useState("")

  const { name, address, email, contact } = state

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleInputChange = (e) => {
    let { name, value } = e.target;
    setState({ ...state, [name]: value})
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!name || !address || !email || !contact) {
      setError("All field are required")
    } else {
      dispatch(addUser(state))
      navigate("/")
      setError("")
    }
  }

  return (
    <Container maxWidth="lg" sx={{ mt: 5 }} >
      <h2>Tambah Data User</h2>
      <Button 
        variant="outlined" 
        color="secondary"
        onClick={() => navigate("/")}
      >
        Go Back
      </Button>

      {error && <Alert severity="error" align="center" sx={{width: "250px", mx: "auto", my: 3}}>{error}</Alert>}
      <Box
        component="form"
        sx={{
          '& > :not(style)': { m: 1, width: '100ch' },
        }}
        noValidate
        autoComplete="off"
        onSubmit={ handleSubmit }
      >
        <TextField 
          id="standard-basic" 
          label="Name" 
          variant="standard"
          type="text"
          name="name"
          value={ name }
          onChange={ handleInputChange }
        />
        <TextField 
          id="standard-basic" 
          label="Address" 
          variant="standard"
          type="text"
          name="address"
          value={ address } 
          onChange={ handleInputChange }
        />
        <TextField 
          id="standard-basic" 
          label="Email" 
          variant="standard"
          type="email"
          name="email"
          value={ email } 
          onChange={ handleInputChange }
        />
        <TextField 
          id="standard-basic" 
          label="Contact" 
          variant="standard"
          type="number"
          name="contact"
          value={ contact } 
          onChange={ handleInputChange }
        />
        
        <Button 
          variant="outlined" 
          color="primary"
          type="submit"
          sx={{ maxWidth: "400px" }}
        >
          Save
        </Button>
      </Box>
    </Container>
  )
}

export default AddUser