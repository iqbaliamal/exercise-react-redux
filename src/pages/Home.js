import React, { useEffect } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button'
import { Container } from '@mui/system';
import { useDispatch, useSelector } from 'react-redux'
import { loadUsers, deleteUser } from '../redux/actions';
import { useNavigate } from 'react-router-dom';

const Home = () => {
  let dispatch = useDispatch();
  const { users } = useSelector((state) => state.data);

  useEffect(() => {
    dispatch(loadUsers());
  });

  const handleDelete = (id) => {
    if(window.confirm("Apakah anda yakin untuk menghapus data?")) {
      dispatch(deleteUser(id));
    }
  }

  const navigate = useNavigate();

  return (
    <Container maxWidth="lg" sx={{ mt: 5 }}>
      <h1>Exercise Redux Kelompok 01</h1>
      <Button 
        variant="contained"
        color="primary"
        onClick={() => navigate("/addUser")}
      >
        Add User
      </Button>
      <TableContainer component={Paper} sx={{ m: "auto" }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="center">Email</TableCell>
              <TableCell align="center">Contact</TableCell>
              <TableCell align="center">Address</TableCell>
              <TableCell align="center">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow
                key={user.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {user.name}
                </TableCell>
                <TableCell align="center">{user.address}</TableCell>
                <TableCell align="center">{user.email}</TableCell>
                <TableCell align="center">{user.contact}</TableCell>
                <TableCell align="center">
                  <Button 
                    variant="outlined" 
                    color="primary" 
                    sx={{ mx: 1 }}
                    onClick={ () => navigate(`/editUser/${user.id}`)}
                  >
                    Edit
                  </Button>
                  <Button 
                    variant="outlined" 
                    color="error"
                    onClick={ () => handleDelete(user.id) }
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  )
}

export default Home